Notes from JavaScript Security Bachaav
--------------------------------------

We had a great session by Ahamed Nafeez [twitter](https://twitter.com/skeptic_fx "Ahamed Nafeez's Twitter") and [blog](blog.skepticfx.com "Ahamed Nafeez's blog") on JavaScript security.

These notes are just a starting point for our journey in mastering JavaScript security. We hope that this particular session is conducted for more people and also be able to get him to come back and cover more topics that emerge of out this and do more levels in JavaScript Security. 

I have tried to add the keywords of the concepts and added the links to the references mentioned. Please feel free to edit, update and correct any errors that might have creeped in. 

Some important JavaScript concepts for this session
===================================================

* typeof - [typeof MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof "Mozilla Developer Network page for typeof")
* instanceof - [instanceof MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/instanceof "Mozilla Developer Network page for instanceof")
    * Additional reading for understanding [JavaScript primitives](http://skilldrick.co.uk/2011/09/understanding-typeof-instanceof-and-constructor-in-javascript/ "Understanding typeof, instanceof and constructor in JavaScript")
* Anonymous functions - [Anonymous Functions in JavaScript](http://en.wikibooks.org/wiki/JavaScript/Anonymous_Functions "Wikibooks - JavaScript Anonymous Functions")
    * [How does an anonymous function work in JavaScript?](http://stackoverflow.com/questions/1140089/how-does-an-anonymous-function-in-javascript-work "How does an anonymous function in JavaScript work?")
* Prototypal Inheritance - [Protypal Inheritance in JavaScript - Douglas Crockford](http://javascript.crockford.com/prototypal.html "Protypal Inheritance in JavaScript - Douglas Crockford")
    * Good example of [JavaScript\'s Prototype-Based Inheritance](http://stackoverflow.com/questions/2064731/good-example-of-javascripts-prototype-based-inheritance "JavaScript\'s Prototype-Based Inheritence")
* A bit more about [Douglas Crockford](http://javascript.crockford.com/ "JavaScript by Douglas Crockford")
	* [YUI](http://yuilibrary.com/ "YUI - Yahoo's open source JavaScript and CSS library")
	* [YSlow](http://developer.yahoo.com/yslow/ "YSlow")
	* Must read book by Douglas Crockford [Javascript - The Good Parts](http://isbn.net.in/9788184045222 "JavaScript - The Good Parts")

* Another must read book is [The Tangled Web](http://lcamtuf.coredump.cx/tangled/ "Book page for The Tangled Web")
    * Another link. There seems be no Indian edition so far (http://isbn.net.in/9781593273880 "The Tangled Web")

* XSS Horrors - Wasn't able to find any link for this book
* [The Browser Hackers Handbook](http://www.amazon.com/Browser-Hackers-Handbook-Wade-Alcorn/dp/1118662091/ref=sr_1_1?s=books&ie=UTF8&qid=1387824510&sr=1-1&keywords=browser+hackers+handbook "Browser Hacker's Handbook")
* The innerHTML Apocalypse - How mXSS attack change everything we believed to know so far - Dr. Mario Heiderich
    * [Video on Vimeo](http://vimeo.com/76222967 "The inner HTML Apocalypse - mXSS")
    * [Paper on mXSS attacks](https://cure53.de/fp170.pdf "mXSS Attacks: Attacking well-secured Web-Applications
by using innerHTML Mutations")
* Shift JIS [Shift Japanese Industrial Standards](http://en.wikipedia.org/wiki/Shift_JIS "Wikipedia page on SJIS")
    * [Understanding Character Sets](http://docs.oracle.com/cd/E38689_01/pt853pbr0/eng/pt/tgbl/concept_UnderstandingCharacterSets-0769d6.html)
    * [Byte Eating Characters](http://security.stackexchange.com/questions/9908/multibyte-character-exploits-php-mysql "Multi-byte attacks"
    * [Charset Bypasses](http://capec.mitre.org/data/definitions/80.html)

* Making fun of programming languages, [Gary Bernhardt - WAT](https://www.destroyallsoftware.com/talks/wat)
* Object.freeze() [Freeze function at MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze "Freeze documentation")
* [Annotated ECMAScript 5.1](http://es5.github.io/ "An annotated, hyperlinked, HTML version of Edition 5.1 of the ECMAScript Specification")
* Ariya Hidayat - Sencha Labs [Github](https://github.com/ariya "Github profile"), [Slideshare](http://www.slideshare.net/ariyahidayat "Slideshare profile")
* World of non-alphanumeric javascript
    * [Brain Fuck](http://patriciopalladino.com/blog/2012/08/09/non-alphanumeric-javascript.html)
    * [JS-Non-AlphaNumeric-Reference](https://github.com/ScrimpyCat/JS-Non-AlphaNumeric-Reference)
* Great Wall of XSS
    * [XSS Challenges](http://challenge.hackvertor.co.uk/)
* XSS information on [thespanner.co.uk](http://thespanner.co.uk)
    * [XSS category on The Spanner Website](http://www.thespanner.co.uk/category/xss/)
    * [Hackvector](https://hackvertor.co.uk/help )
* According to Ahamed Nafeez, if you have attackers code running in your website, then it is not your code anymore. 
* David Ross from IE, now works at Google
* Important concept with respect to security is Variables and Global Namespaces.
* [Variable Hoisting](http://designpepper.com/blog/drips/variable-and-function-hoisting)
* [Global Namespace Pollution](http://www.gnucitizen.org/blog/javascript-global-namespace-pollution/)
* A given page, what are the variables being exposed
    for(x in window){
	if(typeof(window[x]) == "number")
		console.log(x)
    	}
* Cookie Tossing Attack
    * [Yummy cookies across domains](https://github.com/blog/1466-yummy-cookies-across-domains)
    * [Hacking Github with Webkit](http://homakov.blogspot.in/2013/03/hacking-github-with-webkit.html)
    * [New Ways I am Going to Hack Your Web App](http://media.blackhat.com/bh-ad-11/Lundeen/bh-ad-11-Lundeen-New_Ways_Hack_WebApp-WP.pdf)
    * [Risk Associated with Cookies](http://resources.infosecinstitute.com/risk-associated-cookies/) * [DOM XSS Wiki](http://code.google.com/p/domxsswiki/wiki)
* Any source which almost directly enters an eval is exploitable most of the times. 
* Whitelisting, whitelisting, whitelisting
* [You are probably misusing DOM text methods](http://benv.ca/2012/10/4/you-are-probably-misusing-DOM-text-methods/)
