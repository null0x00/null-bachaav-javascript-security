1. Why textContent is bad:

var div = document.createElement('div');
div.innerHTML = 'Hello <a href="http://bob.com">Bob</a>!';
console.log(div.textContent);
// Hello Bob!;


var div = document.createElement('div');
div.innerHTML = 
  'Hello <a>&lt;script&gt;alert(&quot;!&quot;)&lt;/script&gt;</a>!';
console.log(div.textContent);
// Hello <script>alert("!")</script>!


 --------------------------------------
 
 
 
function strip(html) {
   var tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return escapeHtml(tmp.textContent||tmp.innerText);
}

var div = document.createElement('div');
div.textContent = '<span>Foo & bar</span>';
console.log(div.innerHTML)
// &lt;span&gt;Foo &amp; bar&lt;/span&gt;

var div = document.createElement('div');
div.appendChild(document.createTextNode('<span>Foo & bar</span>'));
console.log(div.innerHTML)
// &lt;span&gt;Foo &amp; bar&lt;/span&gt;


function escapeHtml(str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
};


var username = '<img src="herp:/" onerror=alert("derp")>';
var profileLink = '<a href="/profile">' + escapeHtml(username) + '</a>';
var div = document.getElementById('target');
div.innerHTML = profileLink;
// <a href="/profile">&lt;img src="herp:/" onerror=alert("derp")&gt;</a>



var userWebsite = '" onmouseover="alert(\'derp\')" "';
var profileLink = '<a href="' + escapeHtml(userWebsite) + '">Bob</a>';
var div = document.getElementById('target');
div.innerHtml = profileLink;
// <a href="" onmouseover="alert('derp')" "">Bob</a>


function escapeHtml(str) {
    return String(str)
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;")
        .replace(/\//g, "&#x2F;")
}
